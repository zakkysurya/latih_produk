<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Stmt\TryCatch;

class KategoriController extends Controller
{

    public function index()
    {
        $data = Kategori::orderBy('created_at', 'DESC')->get();
        $res = [
            'msg' => 'Show all data kategori',
            'data' => $data
        ];

        return response()->json($res, 200);
    }

    public function store(Request $request)
    {
        //validation data
        $validator = Validator::make($request->all(), [
            'nama_kategori' => 'required'
        ], [
            'nama_kategori.required' => 'Nama Kategori wajib diisi'
        ]);

        //validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //validation success
        try {
            $data = Kategori::create($request->all());

            $res = [
                'msg' => 'insert success',
                'data' => $data
            ];

            return response()->json($res, 201);
        } catch (QueryException $e) {
            //show error validation
            return response()->json([
                'msg' => 'insert fail' . $e->errorInfo
            ]);
        }
    }

    public function show($id)
    {
        $data = Kategori::findOrFail($id);
        $res = [
            'msg' => 'detail data',
            'data' => $data
        ];

        return response()->json($res, 201);
    }

    public function update(Request $request, $id)
    {
        //mencari data yang akan diupdate
        $data = Kategori::findOrFail($id);

        // true - validation
        $validator = Validator::make($request->all(), [
            'nama_kategori' => 'required'
        ], [
            'nama_kategori.required' => 'Nama Kategori wajib diisi'
        ]);

        // if validator error
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //success validation
        try {
            // perubahan dsni untuk data yang sudah diambil
            $data->update($request->all());

            $res = [
                'msg' => 'update success',
                'data' => $data
            ];

            return response()->json($res, 201);
        } catch (QueryException $e) {
            //show error validation
            return response()->json([
                'msg' => 'update fail' . $e->errorInfo
            ]);
        }
    }

    public function destroy($id)
    {
        //mencari data yang akan dihapus
        $data = Kategori::findOrFail($id);

        //success validation
        try {
            // perubahan dsni untuk data yang sudah diambil lalu didelete
            $data->delete();

            $res = [
                'msg' => 'delete success'
            ];

            return response()->json($res, 200);
        } catch (QueryException $e) { //fail validation

            return response()->json([
                'msg' => 'delete fail' . $e->errorInfo
            ]);
        }
    }

    public function search($request)
    {
        $data = Kategori::where('nama_kategori', 'like', '%' . $request . "%")->paginate();

        return response()->json($data);
    }
}
