<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Kategori;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Stmt\TryCatch;

class ProdukController extends Controller
{

    public function index()
    {
        // $data = Produk::orderBy('created_at', 'DESC')->get();
        $data = Produk::addSelect([
            'kategori_id' => Kategori::select('nama_kategori')
                ->whereColumn('kategori_id', 'kategori.id')
        ])->get();
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        //validation data
        $validator = Validator::make($request->all(), [
            'nama_produk' => 'required',
            'kategori_id' => 'required',
            'harga' => 'required'
        ], [
            'nama_produk.required' => 'Nama Produk wajib diisi',
            'kategori_id.required' => 'Kategori wajib dipilih',
            'harga.required' => 'Harga wajib diisi'
        ]);

        //validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //validation success
        try {
            $data = Produk::create($request->all());

            $res = [
                'msg' => 'insert success',
                'data' => $data
            ];

            return response()->json($res, 201);
        } catch (QueryException $e) {
            //show error validation
            return response()->json(['msg' => 'insert fail' . $e->errorInfo]);
        }
    }

    public function show($id)
    {

        $data = Produk::findOrFail($id);
        $res = [
            'msg' => 'detail data',
            'data' => $data
        ];
        return response()->json($data, 201);
    }

    public function update(Request $request, $id)
    {
        //mencari data untuk update
        $data = Produk::findOrFail($id);

        //validation data
        $validator = Validator::make($request->all(), [
            'nama_produk' => 'required',
            'kategori_id' => 'required',
            'harga' => 'required'
        ], [
            'nama_produk.required' => 'Nama Produk wajib diisi',
            'kategori_id.required' => 'Kategori wajib dipilih',
            'harga.required' => 'Harga wajib diisi'
        ]);

        //validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //validation success
        try {
            $data->update($request->all());

            $res = [
                'msg' => 'update success',
                'data' => $data
            ];

            return response()->json($res, 201);
        } catch (QueryException $e) {
            //show error validation
            return response()->json([
                'msg' => 'update fail' . $e->errorInfo
            ]);
        }
    }

    public function destroy($id)
    {
        $data = Produk::findOrFail($id);

        try {
            $data->delete();

            $res = [
                'msg' => 'delete success'
            ];

            return response()->json($res, 200);
        } catch (QueryException $e) {
            return response()->json([
                'msg' => 'delete fail' . $e->errorInfo
            ]);
        }
    }

    public function search($request)
    {
        $data = Produk::where('nama_produk', 'like', '%' . $request . "%")->paginate();

        return response()->json($data);
    }
}
