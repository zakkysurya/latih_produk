<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;

    protected $table = 'produks';
    protected $primaryKey = 'id_produk';

    protected $fillable = ['nama_produk', 'kategori_id', 'harga'];

    public function nama_kategori()
    {
        return $this->belongsTo('App\Kategori');
    }
}
