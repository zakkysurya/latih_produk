
//Kategori
//SET
export function set_form_insert_kategori(){
    //change label form
    const labelForm = document.querySelector("#modalKategoriLabel");
    labelForm.innerHTML = 'Tambah Kategori';
    //reset form
    const input = document.querySelector("#inputNamaKategori");
    input.value = '';
    //change button
    $(".btn-insert-kategori").show();
    $(".btn-update-kategori").hide();
    
}
export async function set_form_update_kategori(id){
    const inpSearchKategori = document.querySelector('#input-search-kategori');
    inpSearchKategori.value = '';
    //change label form
    const labelForm = document.querySelector("#modalKategoriLabel");
    labelForm.innerHTML = 'Update Kategori';
     //set data form
    const input = document.querySelector("#inputNamaKategori");
    const got = await get_data_update(id);
    input.value = got.nama_kategori; 
    //change button
    $(".btn-update-kategori").show();
    $(".btn-insert-kategori").hide();
}
export async function set_data_table(){
    //try if nothing error from new Error
    try{
        const got = await get_all_data();
        let html = '';
        const table = document.querySelector('#tbl-kategori');
        got.data.forEach((row, num) => {
            html += html_kategori(row, (num+1));
        });
        table.innerHTML = html;

    }catch(e){
    //stop and show error from new Error
        alert(e);
    }

}

//GET
export async function get_data_update(id){
    //get data api
    const data = await fetch('http://127.0.0.1:8000/api/kategori/'+id);
    //data to be json
    const res = await data.json();
    //return data json
    return res.data;
}
function get_all_data(){
    //get data api
    return fetch('http://127.0.0.1:8000/api/kategori', { 
       method: 'GET', 
       headers:{'Accept' : 'application/json', 'Content-type' : 'application/json'}})
   .then(res=>{
       //validation if error
       if(!res.ok){
           //throw to new Error
           throw new Error(res.statusText);
       }
        //if nothing error data to be json
        return res.json();
   })
   .then(res => {
        //get data json
        return res;
   });
}

export const insertData = function (url, data){
    //post data api
    fetch(url, {
        method:'POST',
        headers:{'Content-type' : 'application/json'},
        body : JSON.stringify(data)
    }).then(res => {
         //validation if error
       if(!res.ok){
        //throw to new Error
        throw new Error(res.statusText);
    }
     //if nothing error data to be json
     return res.json();
    }).then(res=>{
        //hide moda with jquery
        $("#modalKategori").modal('hide');
        //refresh data
        set_data_table();
    });

}

export const updateData = function(url, data){
    //put data api
    fetch(url, {
        method:'PUT',
        headers:{'Content-type' : 'application/json'},
        body : JSON.stringify(data)
    }).then(res => {
        //validation if error
        if(!res.ok){
            //throw to new Error
            throw new Error(res.statusText);
        }
         //if nothing error data to be json
         return res.json();
    }).then(res=>{
        //hide modal with jquery
        $("#modalKategori").modal('hide');
        //refresh data
        set_data_table();
    });

}

export const deleteData = function (url){
    //delete data api
    fetch(url, {
        method:'DELETE'
    })
    .then( res => {
        //validation if error
        if(!res.ok){
        //throw to new Error
        throw new Error(res.statusText);
    }
     //if nothing error data to be json
     return res.json();
    } )
    .then(res => {
        //refresh data
        set_data_table();
    });
}

export const searchData = function(val){
    return fetch('http://127.0.0.1:8000/api/kategori/search/'+val)
    .then(res => {
        if(!res.ok){
            throw new Error(res.statusText);
        }

        return res.json();
    })
    .then(row => {
        let html = '';
        if(row.data.length < 1){
            html += html_data_empty('data tidak ditemukan');
        }else{
            row.data.forEach((r, num)=>{
                html += html_kategori(r, (num+1));
            });
        }
        
        return html;
    });
}

//html
function html_kategori(row, num){
        const nama_kategori = create_FristUpperCase(row.nama_kategori);
    return `<tr>
            <th scope="row">${num}</th>
                <td>${nama_kategori}</td>
                <td>
                    <a href="javascript:;"><span class="badge bg-warning text-dark delete-kategori" data-id="${row.id}" >hapus</span></a>
                    <a href="javascript:;"><span class="badge bg-info text-dark update-kategori" data-id="${row.id}" data-bs-toggle="modal" data-bs-target="#modalKategori">ubah</span></a>
                </td>
            </tr>`;
}

function html_data_empty(msg){
    return `<tr>
            <td colspan='3'>
                <div class="alert alert-warning text-center" role="alert">
                    ${msg}
                </div>
            </td>
            </tr>`;
}

function create_FristUpperCase(row){
    return row.replace(/^\w|\s[a-z]/g, function(a) {return a.toUpperCase()});
}