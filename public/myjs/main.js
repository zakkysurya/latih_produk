import { set_data_table as get_data_kategori, insertData, updateData, deleteData, searchData } from './kategori.js';
import { set_form_insert_kategori, set_form_update_kategori } from './kategori.js';
import { set_data_table as get_data_produk, insertDataProduk, updateDataProduk, deleteDataProduk, searchDataProduk } from './produk.js';
import { set_form_insert_produk, set_form_update_produk, set_selOpt_Kategori } from './produk.js';

get_data_kategori();
get_data_produk();

//Kategori event handler
document.addEventListener('click', async function(e){

    if(e.target.classList.contains('update-kategori')) {
        //get id value
        const id = e.target.dataset.id;
        // set id in btn update
        const set_btn = document.querySelector('.btn-update-kategori');
        set_btn.dataset.id = id;
        //fetch to api
        set_form_update_kategori(id);

    }else if(e.target.classList.contains('delete-kategori')) {
        //get id value
        const id = e.target.dataset.id;
        //fetch to api
        deleteData('http://127.0.0.1:8000/api/kategori/'+id);
        set_selOpt_Kategori();

    }else if(e.target.classList.contains('tambah-kategori')){
        //set form insert
        set_form_insert_kategori();
          
    }else if(e.target.classList.contains('btn-insert-kategori')){
        //get input value
        const val = document.querySelector('#inputNamaKategori').value;
        //create object
        let data = { 'nama_kategori':val.toLowerCase() };
        //fetch to api
        insertData('http://127.0.0.1:8000/api/kategori', data);
        set_selOpt_Kategori();

    }else if(e.target.classList.contains('btn-update-kategori')){
        //get id value
        const id = e.target.dataset.id;
        //get input value
        const val_nama_kategori = document.querySelector('#inputNamaKategori').value;
        //create object
        let data = { 'nama_kategori':val_nama_kategori.toLowerCase() };
        //fetch to api
        updateData('http://127.0.0.1:8000/api/kategori/'+id, data);
        set_selOpt_Kategori();

    }
});

const inpSearchKategori = document.querySelector('#input-search-kategori');
inpSearchKategori.addEventListener("keyup", async function(){

    //set table if value search is empty
    if(this.value.length < 1 ){
        return get_data_kategori();
    }

    //get data api
    const getDataSearch = await searchData(this.value);

    //set data api
    const table = document.querySelector('#tbl-kategori');
    table.innerHTML = getDataSearch;
});

//Produk event handler - next
document.addEventListener('click', function(e){

    if(e.target.classList.contains('tambah-produk')){

        set_form_insert_produk();

    }else if(e.target.classList.contains('update-produk')){
        //get id value
        const id = e.target.dataset.id;
        // set id in btn update
        const set_btn = document.querySelector('.btn-update-produk');
        set_btn.dataset.id = id;
        //fetch to api
        set_form_update_produk(id);

    }else if(e.target.classList.contains('delete-produk')){
        const id = e.target.dataset.id;
        deleteDataProduk(id);

    }else if(e.target.classList.contains('btn-insert-produk')){
        //get input value
        const inputNamaProduk = document.querySelector("#nama_produk").value;
        const inputHarga = document.querySelector("#harga").value;
        const inputKategori = document.querySelector("#kategori").value;
        //create object
        let data = {
            'nama_produk':inputNamaProduk.toLowerCase(),
            'kategori_id':inputKategori,
            'harga':inputHarga.toLowerCase()
        };
        //fetch to api
        insertDataProduk('http://127.0.0.1:8000/api/produk', data);

    }else if(e.target.classList.contains('btn-update-produk')){
        //get id value
        const id = e.target.dataset.id;
        //get input value
        const inputNamaProduk = document.querySelector("#nama_produk").value;
        const inputHarga = document.querySelector("#harga").value;
        const inputKategori = document.querySelector("#kategori").value;
        //create object
        let data = {
            'nama_produk':inputNamaProduk.toLowerCase(),
            'kategori_id':inputKategori,
            'harga':inputHarga.toLowerCase()
        };
        //fetch to api
        updateDataProduk('http://127.0.0.1:8000/api/produk/'+id, data);

    }

})

const inpSearchProduk = document.querySelector('#input-search-produk');
inpSearchProduk.addEventListener("keyup", async function(){

    //set table if value search is empty
    if(this.value.length < 1 ){
        return get_data_produk();
    }

    //get data api
    const getDataSearch = await searchDataProduk(this.value);

    //set data api
    const table = document.querySelector('#tbl-produk');
    table.innerHTML = getDataSearch;
});