//produk
//SET
export function set_form_insert_produk(){
    //change label form
    const labelForm = document.querySelector("#modalProdukLabel");
    labelForm.innerHTML = 'Tambah Produk';
    //reset form
    const inputNamaProduk = document.querySelector("#nama_produk");
    const inputHarga = document.querySelector("#harga");
    const inputKategori = document.querySelector("#kategori");
    inputNamaProduk.value = '';
    inputHarga.value = '';
    inputKategori.value = '';
    set_selOpt_Kategori();
    //change button
    $(".btn-insert-produk").show();
    $(".btn-update-produk").hide();
    
}
export async function set_form_update_produk(id){
    const inpSearchProduk = document.querySelector('#input-search-produk');
    inpSearchProduk.value = '';
    //change label form
    const labelForm = document.querySelector("#modalProdukLabel");
    labelForm.innerHTML = 'Update Produk';
    //set select option kategori 
    set_selOpt_Kategori();
    //set data form
    const inputNamaProduk = document.querySelector("#nama_produk");
    const inputHarga = document.querySelector("#harga");
    const inputKategori = document.querySelector("#kategori");
    const got = await get_data_update(id);
    inputNamaProduk.value = got.nama_produk;
    inputHarga.value = got.harga;
    inputKategori.value = got.kategori_id;
    //change button
    $(".btn-update-produk").show();
    $(".btn-insert-produk").hide();
}
export async function set_data_table(){
    //try if nothing error from new Error
    try{
        const got = await get_all_data('http://127.0.0.1:8000/api/produk/');
        
        let html = '';
        const namaKategori ='';
        const table = document.querySelector('#tbl-produk');
        got.forEach((row, num) => {
            html += html_produk(row,(num+1));
        });
        table.innerHTML = html;

    }catch(e){
    //stop and show error from new Error
        alert(e);
    }

}
export async function set_selOpt_Kategori(){
    //get id select
    const table = document.querySelector('#kategori');

    let got = await get_all_data('http://127.0.0.1:8000/api/kategori/');
    let html = '<option>--Pilih--</option>';
    got.data.forEach(row=>{
        html += html_selOpt_kategori(row);
    });
    table.innerHTML = html;
}

//GET
async function get_data_update(id){
    //get data api
    const data = await fetch('http://127.0.0.1:8000/api/produk/'+id)
    //data to be json
    const res = await data.json();
    //return data json
    return res;
}
function get_all_data(url){
    //get data api
    return fetch(url, { 
       method: 'GET', 
       headers:{'Accept' : 'application/json', 'Content-type' : 'application/json'}})
   .then(res=>{
       //validation if error
       if(!res.ok){
           //throw to new Error
           throw new Error(res.statusText);
       }
        //if nothing error data to be json
        return res.json();
   })
   .then(res => {
        //get data json
        return res;
   });
}

export const insertDataProduk = function (url, data){
    //post data api
    fetch(url, {
        method:'POST',
        headers:{'Content-type' : 'application/json'},
        body : JSON.stringify(data)
    }).then(res => {
         //validation if error
       if(!res.ok){
        //throw to new Error
        throw new Error(res.statusText);
    }
     //if nothing error data to be json
     return res.json();
    }).then(res=>{
        //hide moda with jquery
        $("#modalProduk").modal('hide');
        //refresh data
        set_data_table();
    });

}

export const updateDataProduk = function(url, data){
    //put data api
    fetch(url, {
        method:'PUT',
        headers:{'Content-type' : 'application/json'},
        body : JSON.stringify(data)
    }).then(res => {
        //validation if error
        if(!res.ok){
            //throw to new Error
            throw new Error(res.statusText);
        }
         //if nothing error data to be json
         return res.json();
    }).then(res=>{
        //hide modal with jquery
        $("#modalProduk").modal('hide');
        //refresh data
        set_data_table();
    });

}

export const deleteDataProduk = function (id){
    //delete data api
    fetch('http://127.0.0.1:8000/api/produk/'+id, {
        method:'DELETE'
    })
    .then( res => {
        //validation if error
        if(!res.ok){
        //throw to new Error
        throw new Error(res.statusText);
    }
     //if nothing error data to be json
     return res.json();
    } )
    .then(res => {
        //refresh data
        set_data_table();
    });
}

export const searchDataProduk = function(val){
    return fetch('http://127.0.0.1:8000/api/produk/search/'+val)
    .then(res => {
        if(!res.ok){
            throw new Error(res.statusText);
        }

        return res.json();
    })
    .then(row => {
        let html = '';
        if(row.data.length < 1){
            html += html_data_empty('data tidak ditemukan', '5');
        }else{
            row.data.forEach((r, num)=>{
                html += html_produk(r, (num+1));
            });
        }
        
        return html;
    });
}

//html
  function html_produk(row, num){
        return `<tr>
                <th scope="row">${num}</th>
                <td>${row.nama_produk}</td>
                <td>${row.harga}</td>
                <td>${row.kategori_id}</td>
                <td>
                    <a href="javascript:;"><span class="badge bg-warning text-dark delete-produk" data-id="${row.id_produk}" >hapus</span></a>
                    <a href="javascript:;"><span class="badge bg-info text-dark update-produk" data-id="${row.id_produk}" data-bs-toggle="modal" data-bs-target="#modalProduk">ubah</span></a>
                </td>
            </tr>`;
}

function html_data_empty(msg, rows){
    return `<tr>
            <td colspan='${rows}'>
                <div class="alert alert-warning text-center" role="alert">
                    ${msg}
                </div>
            </td>
            </tr>`;
}

function html_selOpt_kategori(row){
    return `<option value=${row.id}>${row.nama_kategori}</option>`
}

function create_FristUpperCase(row){
    return row.replace(/^\w|\s[a-z]/g, function(a) {return a.toUpperCase()});
}