<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="{{url('bootstrap-5/css/bootstrap.min.css')}}" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="{{url('bootstrap-5/js/bootstrap.bundle.min.js')}}" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

    <script src="{{url('myjs/jquery-3.5.1.min.js')}}"></script>
    <script type="module" src="{{url('myjs/main.js')}}"></script>

</head>

<body>
    <div class="container mt-3">
        <div class="row">
            <div class="col-8">
                <div class="row">
                    <div class="col-8">
                        <h3>Produk</h3>
                    </div>
                    <div class="col-4">
                        <input class="form-control" id="input-search-produk" placeholder="Cari : Nama Produk">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <table class="table">
                            <thead class="table-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nama Produk</th>
                                    <th scope="col">Harga</th>
                                    <th scope="col">Kategori</th>
                                    <th scope="col">Aksi
                                        <a href="javascript:;"><span class="badge bg-primary tambah-produk" data-bs-toggle="modal" data-bs-target="#modalProduk">tambah</span></a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="tbl-produk">

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="col-4">
                <div class="row">
                    <div class="col-6">
                        <h3>Kategori</h3>
                    </div>
                    <div class="col-6">
                        <input class="form-control" id="input-search-kategori" placeholder="Cari : Nama Kategori">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <table class="table">
                            <thead class="table-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nama Kategori</th>
                                    <th scope="col">Aksi
                                        <a href="javascript:;"><span class="badge bg-primary tambah-kategori" data-bs-toggle="modal" data-bs-target="#modalKategori">tambah</span></a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="tbl-kategori">

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>

    </div>

    <!-- Produk -->
    <!-- Modal -->
    <div class="modal fade" id="modalProduk" tabindex="-1" role="dialog" aria-labelledby="modalProdukLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalProdukLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="nama_produk">Nama Produk</label>
                            <input type="text" class="form-control" id="nama_produk">
                        </div>
                        <div class="form-group">
                            <label for="harga">Harga</label>
                            <input type="text" class="form-control" id="harga">
                        </div>
                        <div class="form-group">
                            <label for="kategori">Kategori</label>
                            <select id="kategori" class="form-control">

                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary btn-insert-produk">Simpan</button>
                    <button type="button" class="btn btn-primary btn-update-produk" data-id="">Update</button>
                </div>
            </div>
        </div>
    </div>

    <!-- kategori -->
    <!-- Modal -->
    <div class="modal fade" id="modalKategori" tabindex="-1" role="dialog" aria-labelledby="modalKategoriLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalKategoriLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="inputNamaKategori">Nama Kategori</label>
                            <input type="text" class="form-control" id="inputNamaKategori" placeholder="Sembako...">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary btn-insert-kategori">Simpan</button>
                    <button type="button" class="btn btn-primary btn-update-kategori" data-id="">Update</button>
                </div>
            </div>
        </div>
    </div>

</body>

</html>