<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\KategoriController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//produk
Route::get('/produk', [ProdukController::class, 'index']);
Route::post('/produk', [ProdukController::class, 'store']);
Route::get('/produk/{id}', [ProdukController::class, 'show']);
Route::put('/produk/{id}', [ProdukController::class, 'update']);
Route::delete('/produk/{id}', [ProdukController::class, 'destroy']);
Route::get('/produk/search/{nama_produk}', [ProdukController::class, 'search']);

//kategori
Route::resource('kategori', KategoriController::class)->only([
    'index', 'store', 'show', 'update', 'destroy', 'search'
]);
Route::get('/kategori/search/{nama_kategori}', [KategoriController::class, 'search']);
// Route::get('/kategori', [KategoriController::class, 'index']);
// Route::post('/kategori', [KategoriController::class, 'store']);
// Route::get('/kategori/{id}', [KategoriController::class, 'show']);
// Route::put('/kategori/{id}', [KategoriController::class, 'update']);
// Route::delete('/kategori/{id}', [KategoriController::class, 'destroy']);
